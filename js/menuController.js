"use strict";

function MenuController() {
    var self = this;

    var service = new DataAPI();

    self.bind = function (element) {
        menuItemList.forEach(element => {
            element.addEventListener('click', callSection, element);
        });
        self.display = element.find('[data-role="menuItems"]');
    }

    $(function getMenuData() {
        service.getData(setMenu, 'menu.php');
    });

    function setMenu(data) {
        menuHomeItem.firstElementChild.textContent = data[0].name;
        menuMoviesItem.firstElementChild.textContent = data[1].name;
        menuSettingsItem.firstElementChild.textContent = data[2].name;

        menuNav();
    }

    function removeClassActive() {
        menuItemListButton.forEach(element => {
            element.classList.remove('active');
        });
    }

    function callSection(element) {
        removeClassActive();

        var el = element.currentTarget;
        el.firstElementChild.classList.add('active');

        switch (el.id) {
            case 'menuMovies':
                sectionHome.style.display = 'none';
                sectionMovies.style.display = 'block';
                sectionSettings.style.display = 'none';

                document.querySelector('#movies button').focus();
                break;
            case 'menuSettings':
                sectionHome.style.display = 'none';
                sectionMovies.style.display = 'none';
                sectionSettings.style.display = 'block';

                document.querySelector('#settings button.active').focus();
                break;
            case 'menuHome':
            default:
                sectionHome.style.display = 'block';
                sectionMovies.style.display = 'none';
                sectionSettings.style.display = 'none';

                btnMenu.focus();
                break;
        }
        closeMenu();
    }

    function menuNav() {
        var list = document.querySelector('.sidenav ul');
        var items = list.querySelectorAll('.sidenav button');
        var codes = { 38: -1, 40: 1 };
        for (var i = 0; i < items.length; i++) {
            items[i].count = i;
        }
        function handlekeys(ev) {
            removeClassActive();
            var keycode = ev.keyCode;
            if (codes[keycode]) {
                var t = ev.target;
                if (t.count !== undefined) {
                    if (items[t.count + codes[keycode]]) {
                        items[t.count + codes[keycode]].focus();
                    }
                }
            }
        }
        items[0].focus();
        menuUl.addEventListener('keyup', handlekeys);
    }

    return self;
}