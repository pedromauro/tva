"use strict";

$(function () {
    btnMenu.style.display = 'none';

    $('[data-controller]').each(function (index, element) {
        var target = $(element);
        var controllerName = target.data().controller;
        var controllerInstance = eval('new ' + controllerName + '()');
        controllerInstance.bind(target);
    });
    btnMenu.addEventListener('click', openMenu);
});

function openMenu() {
    overlay.style.display = 'block';
    menu.style.width = '275px';

    document.querySelector('.sidenav button.active').focus();
}

var closeMenu = function () {
    menu.style.width = '0';
    overlay.style.display = 'none';
}