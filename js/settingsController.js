"use strict";

function SettingsController() {
    var self = this;

    self.bind = function (element) {
        settingsList.forEach(element => {
            element.addEventListener('click', setLanguage, element);
        });
        self.display = element.find('[data-role="settingsItems"]');
    }

    $(function setSettingsList() {
        changeLanguage();
        settingsNav();
    });

    function removeSettingsClassActive() {
        menuItemSettingsButton.forEach(element => {
            element.classList.remove('active');
        });
    }

    function setLanguage(element) {
        removeSettingsClassActive();
        var langCode;
        var el = element.currentTarget;
        el.firstElementChild.classList.add('active');
        switch (el.id) {
            case 'en':
                langCode = 'en';
                break;
            case 'es':
                langCode = 'es';
                break;
        }
        localStorage.setItem('lang', langCode);
        changeLanguage();
    }

    function changeLanguage() {
        try {
            var langCode = localStorage.getItem('lang');
        } catch (error) {
            alert('Your browser is blocking third-party cookies. Using default language.');
            langCode = 'en';
        }
        var langs = ['es'];
        var translate = function (data) {
            $("[key]").each(function (index) {
                var value = data[$(this).attr('key')];
                $(this).html(value);
            });
        };
        if (langs.includes(langCode)) {
            if ($.getJSON('js/' + langCode + '.json', translate).statusText !== 'error') {
                $.getJSON('js/' + langCode + '.json', translate);
            } else {
                alert('Failed to load translation file. Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, https.');
            }
            settingsUl.lastElementChild.querySelector('button').classList.add('active');
            settingsUl.firstElementChild.querySelector('button').classList.remove('active');
            settingsUl.lastElementChild.querySelector('button').focus();
        } else {
            if ($.getJSON('js/en.json', translate).statusText !== 'error') {
                $.getJSON('js/en.json', translate);
            } else {
                alert('Failed to load translation file. Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, https.');
            }
            settingsUl.firstElementChild.querySelector('button').classList.add('active');
            settingsUl.lastElementChild.querySelector('button').classList.remove('active');
            settingsUl.firstElementChild.querySelector('button').focus();
        }
    }

    function settingsNav() {
        var list = document.querySelector('#settings ul');
        var items = list.querySelectorAll('#settings button');
        var codes = { 38: -1, 40: 1 };
        for (var i = 0; i < items.length; i++) {
            items[i].count = i;
        }
        function handlekeys(ev) {
            var keycode = ev.keyCode;
            if (codes[keycode]) {
                var t = ev.target;
                if (t.count !== undefined) {
                    if (items[t.count + codes[keycode]]) {
                        items[t.count + codes[keycode]].focus();
                    } else if (keycode === 38) {
                        btnMenu.focus();
                    }
                }
            }
        }
        items[0].focus();
        settingsUl.addEventListener('keyup', handlekeys);
    }
    return self;
}