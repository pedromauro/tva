"use strict";

function MoviesController() {
    var self = this;

    var service = new DataAPI();

    self.bind = function (element) {
        self.display = element.find('[data-role="movieItems"]');
    }

    $(function getMoviesData() {
        service.getData(setMovies, 'movies.php');
    });

    function setMovies(data) {
        data.forEach(element => {
            var movie = new Movie(element.id, element.name, element.images.keyartUrl);
            setMovieList(movie);
        });
        gridNav();

        openMenu();
    }

    function setMovieList(data) {
        var li = document.createElement('li');

        var button = document.createElement('button');
        button.setAttribute('id', data.id);
        button.setAttribute('title', data.name);

        var image = document.createElement('img');
        image.setAttribute('src', data.image);

        button.appendChild(image);
        li.appendChild(button);
        moviesUl.appendChild(li);

        btnMenu.style.display = 'block';
    }

    function gridNav() {
        var list = document.querySelector('#movies ul');
        var items = list.querySelectorAll('#movies button');
        var codes = { 38: -5, 40: 5, 39: 1, 37: -1 };
        for (var i = 0; i < items.length; i++) {
            items[i].count = i;
        }
        function handlekeys(ev) {
            var keycode = ev.keyCode;
            if (codes[keycode]) {
                var t = ev.target;
                if (t.count !== undefined) {
                    if (items[t.count + codes[keycode]]) {
                        items[t.count + codes[keycode]].focus();
                    } else if (keycode === 38) {
                        btnMenu.focus();
                    }
                }
            }
        }
        items[0].focus();
        moviesUl.addEventListener('keyup', handlekeys);
    }

    return self;
}