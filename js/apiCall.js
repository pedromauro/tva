
function DataAPI() {
    var self = this;

    self.getData = function (_callback, methodName) {
        var http = new XMLHttpRequest();
        var url = 'http://testapps.tvappagency.com/sobrinho/stv_test/' + methodName;
        var method = 'GET';
        var data = [];

        http.open(method, url);
        http.setRequestHeader('ovs-token', 'VGhpcyBpcyBhIFRWQXBwIEFnZW5jeSB2YWxpZCB0b2tlbi4=');
        http.setRequestHeader('bpm-environment', 'test-api')
        http.send();
        http.onreadystatechange = function () {
            if (http.responseText !== '')
                var responseData = JSON.parse(http.responseText);
            if (http.readyState == XMLHttpRequest.DONE && http.status === 200) {
                data = responseData.data;
                _callback(data);
            } else if (http.readyState === XMLHttpRequest.DONE) {
                alert(responseData.description);
            }
        };
    };
}