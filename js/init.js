"use strict";

var btnMenu = document.querySelector('#menuBtn');
var overlay = document.querySelector('.overlay');

var controllers = document.querySelectorAll('[data-controller]');

var menu = document.querySelector('#menu');
var menuUl = menu.querySelector('ul');
var menuHomeItem = document.querySelector('#menuHome');
var menuMoviesItem = document.querySelector('#menuMovies');
var menuSettingsItem = document.querySelector('#menuSettings');
var menuItemList = document.querySelectorAll('.menuItem');
var menuItemListButton = document.querySelectorAll('.itemButton');

var sectionHome = document.querySelector('#home');
var sectionMovies = document.querySelector('#movies');
var sectionSettings = document.querySelector('#settings');

var moviesUl = sectionMovies.firstElementChild;
var settingsUl = sectionSettings.querySelector('ul');
var settingsList = document.querySelectorAll('.settingsItem');
var menuItemSettingsButton = document.querySelectorAll('.settingsButton');
